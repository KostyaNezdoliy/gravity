FUNCTION_BLOCK fbLight
VAR_INPUT
    btn : bool;
END_VAR
VAR_IN_OUT
    dimer: real;
END_VAR
VAR_OUTPUT
    dimer_save: real;
    dimer_usint : usint;
END_VAR
VAR
   Flag, Flag_1: int;
   proc: real:= 50.0;
   step,
   value: real;
   FTRIG: F_TRIG;
   RTRIG: R_TRIG;
   TimerTON: TON;
   i: usint;
   PuskStop: bool;
END_VAR
   RTRIG(CLK:= btn);
   FTRIG(CLK:= btn);
   TimerTON(IN:= btn, PT:=T#1s);
   IF dimer = 100.0 AND Flag = 0 THEN
      PuskStop:= false; Flag:= 1;
      dimer_usint:= 255;
   ELSIF dimer = 0.0 AND Flag = 1 THEN
      PuskStop:= false; Flag:= 0;
   END_IF;
   IF RTRIG.Q THEN
      PuskStop:= true; Flag_1:= 0;
   END_IF;
   IF FTRIG.Q AND Flag_1 = 0 THEN
      IF dimer > 0.0 AND dimer <= 100.0 THEN
         dimer:= 0.0; i:= 0; value:= 1.0;
         dimer_usint:= 0;
      ELSIF dimer = 0.0 THEN
         dimer:= 100.0; i:= 100; value:= -1.0;
         IF dimer_save <> 0.0 THEN
            dimer:= dimer_save; i:= REAL_TO_USINT(dimer_save);
            dimer_usint:= real_to_usint(dimer*2.55);
            value:= 1.0;
         END_IF;
      END_IF;
   ELSIF TimerTON.Q THEN
      Flag_1:= 1;
      IF dimer = 0.0 THEN
         value:= 1.0;
      ELSIF dimer = 100.0 THEN
         value:= -1.0;
      END_IF;
      IF PuskStop = true THEN
         step:= proc/100.0;
         dimer:= dimer + value*(step);
      END_IF;
      IF dimer > 100.0 THEN
         dimer:= 100.0;
      END_IF;
      IF dimer < 0.0  THEN
         dimer:= 0.0;
      END_IF;
      dimer_save:= dimer;
      dimer_usint:= real_to_usint(dimer*2.55);
      i:= real_to_usint(dimer);
   END_IF;
END_FUNCTION_BLOCK

