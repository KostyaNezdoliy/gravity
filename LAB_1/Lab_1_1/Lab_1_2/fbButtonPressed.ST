FUNCTION_BLOCK fbButtonController
VAR_INPUT
  btn: bool;                                // ���������� ������
END_VAR
VAR_OUTPUT
  ShortPress,                               // �������� ��������� �������
  LongPress,                                // �������� ������� �������
  LongPressHold: bool;                      // �������� ������� ������� � ����������
END_VAR
VAR
  T1: int;                                  // ���� ���������� ��������� ������� (���� ����� 1 ������� �������� ���. �� �����������)
  Input_time: real;                         // ���������� ������� ������� ������� ������� ����� �������� � WebMaker
  LongTime_real: real:= 1000;               // ���������� ������� ������� ������� � ������������
  LongTime: TIME;                           // ���������� ������� ������� �������
  PressedTP,                                // ��� ������� ������
  ReleasedTP,                               // ��� ���������� ������
  ShortTP,                                  // ��� ��������� �������
  LongTP: TP;                               // ��� ������� �������
  RTRIG: R_TRIG;
  FTRIG: F_TRIG;
  TimerTON: TON;
  button_status: bool;                      // ��������� ������
END_VAR
IF Input_time <> 0.0 THEN                   // ������� ��������� ������� ������� �������
  LongTime_real:= Input_time*1000.0;
END_IF;
LongTime:= real_to_time(LongTime_real);
RTRIG(CLK:= btn);
FTRIG(CLK:= btn);
TimerTON(IN:= btn, PT:= LongTime);
button_status:= btn;

PressedTP(IN := RTRIG.Q, PT := T#300ms);
ReleasedTP(IN := FTRIG.Q, PT := T#300ms);
ShortTP(IN := ShortPress, PT := T#300ms);
LongTP(IN := LongPress, PT := T#300ms);

LongPress:= false;                          // �������� �������� ������� �������
ShortPress:= false;                         // �������� �������� ��������� �������
LongPressHold:= false;                      // �������� �������� ������� ������� � ����������
IF RTRIG.Q THEN                             // ��� ������� ������ ������� �������� ����� �1
  T1:= 0;
END_IF;
IF FTRIG.Q AND T1 = 0 THEN                  (***������� ��������� �������***)
  ShortPress:= true;
END_IF;
IF TimerTON.Q THEN                          (***������� ������� ������� � ������� � ����������***)
  LongPress:= true;
  LongPressHold:= true; T1:= 1;
END_IF;

END_FUNCTION_BLOCK
